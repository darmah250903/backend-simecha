<?php

use Illuminate\Http\Request;

Route::prefix('v1')->group(function() {

	Route::get('faq', 'FaqController@show');
	Route::get('setting', 'SettingController@show');
	Route::get('kirimemail','DrakeMailController@index');
	Route::post('getcodeverifikasi','authController@sendVerification');
	Route::post('clearcode','authController@clearcode');
	Route::post('verifikasi','authController@verifikasi');
	Route::post('back','authController@back');
	Route::post('register','authController@register');
	Route::post('check','authController@check');
	Route::post('check_signup','authController@checkSignUp');
	Route::post('accForgot','authController@accForgot');
	Route::get('getdatabengkel','authController@getdatabengkel');
	
	Route::prefix('auth')->group(function(){
		// Route::get('verify/{verification_code}', 'AuthController@verifyUser');
		Route::post('register', 'AuthController@register');
		Route::post('login', 'AuthController@login');
		Route::post('recover', 'AuthController@recover');
		Route::post('recover/check', 'AuthController@checkRecover');
		Route::post('recover/send', 'AuthController@postRecover');
	});
	Route::group(['middleware' => ['jwt.auth']], function() {
		
		Route::prefix('auth')->group(function(){
			Route::post('logout', 'AuthController@logout');
		    Route::get('user', 'AuthController@user');
		    Route::get('refresh', 'AuthController@refresh');
		    
		});
		Route::get('getcodeverifikasipassword', 'AuthController@getcodeverifikasipassword');
		Route::post('changepassword', 'AuthController@changepassword');
		Route::post('changephoto', 'AuthController@changephoto');
		Route::post('verifikasipassword', 'AuthController@verifikasipassword');
		Route::post('sendchat', 'ChatController@send');
		
		Route::prefix('admin')->group(function(){
		    Route::post('get_data/pemesanan', 'PemesananController@adminGetDataPemesanan');
		    Route::post('get_data', 'PemesananController@adminGetData');
		    Route::post('changestatus', 'PemesananController@adminChangeStatus');
		    Route::get('{status}/pesanan', 'PemesananController@pesanan');
		    Route::post('detailpesanan', 'PemesananController@detailpesanan');
			Route::post('changestatuspemesanan', 'PemesananController@changestatuspemesanan');
			Route::post('selesaipemesanantambahan', 'PemesananController@selesaipemesanantambahan');
		    
		});
		
		Route::prefix('user')->group(function(){
			Route::post('changestatuspemesanan', 'UserPemesananController@changestatuspemesanan');
		    Route::post('getservice', 'UserPemesananController@getservice');
		    Route::get('checkpemesanan', 'UserPemesananController@checkpemesanan');
		    Route::post('sendpesanan', 'UserPemesananController@sendpesanan');
		    Route::get('get_data', 'UserPemesananController@userGetData');
		    Route::get('getpesanan', 'UserPemesananController@getpesanan');
		    Route::get('getuserdetail', 'UserPemesananController@getuserdetail');
		    Route::post('sendReport', 'PemesananController@sendReport');
		    Route::post('ubahDetail', 'PemesananController@ubahDetail');
		    Route::post('ubahPosisi', 'PemesananController@ubahPosisi');
		    Route::post('inputUlang', 'PemesananController@inputUlang');
		    Route::post('sendRating', 'PemesananController@sendRating');
		});
		
		
	    Route::post('test', 'AuthController@testing');
	    Route::get('block/{uuid}', 'AuthController@block');
	    Route::get('unblock/{uuid}', 'AuthController@unblock');
		Route::post('setting/update', 'SettingController@update');
		
		Route::prefix('service')->group(function(){
			Route::post('index', 'ServiceController@index');
			Route::post('tambah', 'ServiceController@tambah');
			Route::post('edit', 'ServiceController@edit');
			Route::get('getdata', 'ServiceController@getdata');
			Route::get('delete/{uuid}', 'ServiceController@delete');
			Route::get('getdataedit/{uuid}', 'ServiceController@getdataedit');
		});

		Route::prefix('report')->group(function(){
			Route::post('index', 'ReportController@index');
			Route::get('detail/{uuid}', 'ReportController@detail');
		});


	});
});
