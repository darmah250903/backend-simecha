<?php

use App\Models\BengkelType;
use App\Models\Service;
use Illuminate\Database\Seeder;

class BengkelServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::create([
            'bengkel_type_id' => 1,
            'nama' => 'ganti oli',
            'harga' => 100000
        ]);
        Service::create([
            'bengkel_type_id' => 1,
            'nama' => 'strom aki',
            'harga' => 150000
        ]);
        Service::create([
            'bengkel_type_id' => 1,
            'nama' => 'Ban bocor (tubles)',
            'harga' => 15000
        ]);
        Service::create([
            'bengkel_type_id' => 1,
            'nama' => 'Ban bocor (non tubles)',
            'harga' => 15000
        ]);
    }
}
