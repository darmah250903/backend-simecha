<?php

use App\Models\BengkelType;
use Illuminate\Database\Seeder;

class BengkelTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BengkelType::create([
            'nama' => 'Motor'
        ]);
        BengkelType::create([
            'nama' => 'AC'
        ]);
        BengkelType::create([
            'nama' => 'HP'
        ]);
    }
}
