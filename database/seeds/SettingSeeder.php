<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
        	'name' => 'MTI+', 
        	'background' => '#', 
        	'logo_dark' => '#', 
        	'logo_white' => '#', 
        	'description' => 'Cv.MCFLYON TEKNOLOGI INDONESIAx'
        ]);
    }
}
