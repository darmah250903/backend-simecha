<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;
use App\Models\User;
use App\Models\Setting;


class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
        	[
            'name' => 'Administrator',
            'email' => 'darmah250903@gmail.com',
            'password' => bcrypt('12345678'),
            'level' => 'superadmin',
        	]
        );

        Setting::create([
        	'name' => 'MTI+', 
        	'background' => '#', 
        	'logo_dark' => '#', 
        	'logo_white' => '#', 
        	'description' => 'Cv.MCFLYON TEKNOLOGI INDONESIAx'
        ]);

    }
}
