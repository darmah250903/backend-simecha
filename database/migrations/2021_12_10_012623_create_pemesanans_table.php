<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanans', function (Blueprint $table) {
            $table->increments('id');

            $table->uuid('uuid')->unique();
            $table->string('kd_pemesanan')->unique();

            $table->double('lat');
            $table->double('long');

            $table->bigInteger('harga_pemesanan')->nullable();
            $table->bigInteger('harga_perjalanan')->nullable();
            $table->enum('status',[0, 1, 2, 3])->default(0);

            $table->enum('pemesanan_type',[0, 1])->default(0);

            $table->text('alamat');
            $table->text('deskripsi')->nullable();
            $table->text('deskripsi_user')->nullable();


            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade');
            
            $table->integer('admin_id')->unsigned();
            $table->foreign('admin_id')->references('id')->on('users')
            ->onDelete('cascade');
            
            $table->date('tgl_pemesanan');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanans');
    }
}
