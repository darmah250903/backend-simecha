<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (JWTAuth::user()->level != 'admin') {
            return response()->json([
                'status' => false,
                'message' => '404 Not Found'
            ], 404);
        }
        
        return $next($request);
    }
}
