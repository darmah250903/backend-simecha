<?php

namespace App\Http\Controllers;

use App\Models\BengkelType;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Helpers\AppHelper as AH;
use App\Models\bengkel_details;
use App\Mail\Notif;
use App\Models\pemesanan;
use App\Models\PemesananService;
use Auth,Mail;
use Illuminate\Support\Facades\Date;

class UserPemesananController extends Controller
{
    public function getservice(Request $request)
    {
        $data = BengkelType::with('service')->where('id', '=', $request->bengkel_type_id)->first();

        $data->service->map(function($q){
            $q->value = false;
            $q->hargarupiah = AH::rupiah($q->harga);
        });

        return response()->json($data);
    }

    public function sendpesanan(Request $request)
    {



        $data = User::with('bengkel_detail', 'pemesananAdmin')->where('block', '=', '0')->whereHas('bengkel_detail', function($q) use ($request){
            $q->where('kota', '=', $request->kota);
            $q->where('bengkel_type_id', '=', $request->bengkel_id);
            $q->where('status', '=', '1');
        })->get();

        

        //hitung jarak user ke bengkel

        $data->map(function($q) use ($request){
            $theta = $request->long - $q->bengkel_detail->long;
            $miles = (sin(deg2rad($request->lat)) * sin(deg2rad($q->bengkel_detail->lat))) + (cos(deg2rad($request->lat)) * cos(deg2rad($q->bengkel_detail->lat)) * cos(deg2rad($theta)));
            $miles = acos($miles);
            $miles = rad2deg($miles);
            $miles = $miles * 60 * 1.1515;
            // $feet  = $miles * 5280;
            // $yards = $feet / 3;
            $kilometers = $miles * 1.609344;
            // $meters = $kilometers * 1000;
            
            $q->jarak = $kilometers;
        });

        $sort = $data->sortBy('jarak');


        if(count($sort) == 0){
            return response()->json([
                'pesan' => 'tidak ada bengkel'
            ], 400);
        }

        if(intval($sort[0]->jarak) == 0){
            $ongkir = 2000 * 1;
        } else {
            $ongkir = 2000 * intval($sort[0]->jarak);
        }


        // status 0 -> custom && status 1 -> default

        $tra = pemesanan::orderBy('id', 'desc')->first();
        if(!$tra){
            $request->kode_transaksi = str_pad(0, 20, '0', STR_PAD_LEFT);
        } else {
            $request->kode_transaksi = str_pad($tra->id + 1, 20, '0', STR_PAD_LEFT);
        }

        
        if($request->status == '1'){
            $pem = pemesanan::create([
                'kd_pemesanan' => $request->kode_transaksi,
                'lat' => $request->lat,
                'long' => $request->long,
                'long' => $request->long,
                'alamat' => $request->alamat,
                'user_id' => Auth::user()->id,
                'admin_id' => $sort[0]->id,
                'tgl_pemesanan' => Date('Y-m-d'),
                'pemesanan_type' => $request->status,
                'harga_perjalanan' => $ongkir
            ]);

            if($pem){
                for ($i=0; $i < count($request->service); $i++) { 
                    PemesananService::create([
                        'service_id' => $request->service[$i],
                        'pemesanan_id' => $pem->id,
                    ]);
                }

                bengkel_details::where('user_id', '=', $pem->admin_id)->first()->update(['status' => '0']);
                $user = User::where('id', '=', $pem->admin_id)->first();
                
                return response()->json(pemesanan::with(['user', 'admin', 'pemesananservice'])->where('id', '=', $pem->id)->first());
            }
        }else {
            $pem = pemesanan::create([
                'kd_pemesanan' => $request->kode_transaksi,
                'lat' => $request->lat,
                'long' => $request->long,
                'long' => $request->long,
                'alamat' => $request->alamat,
                'deskripsi_user' => $request->deskripsi_user,
                'user_id' => Auth::user()->id,
                'admin_id' => $sort[0]->id,
                'tgl_pemesanan' => Date('Y-m-d'),
                'pemesanan_type' => $request->status,
                'harga_perjalanan' => $ongkir
            ]);
            bengkel_details::where('user_id', '=', $pem->admin_id)->first()->update(['status' => '0']);
            $user = User::where('id', '=', $pem->admin_id)->first();


            return response()->json(pemesanan::with(['user', 'admin'])->where('id', '=', $pem->id)->first());
        }
    }

    public function checkpemesanan()
    {
        $data = pemesanan::where('user_id', '=', Auth::user()->id)->where([['status', '!=', '2'], ['status', '!=', '3']])->get();

        if(count($data) != 0){
            return response()->json(['pesan' => 'anda sedang memesan salah satu service'], 400);
        } else {
            return response()->json(['pesan' => 'sukses'], 200);
        }

    }


    public function userGetData()
    {

        $data = User::with('bengkel_detail')->where('id', '=', Auth::user()->id)->first();

        return Response()->json($data);
    }

    public function getpesanan()
    {
        $data = pemesanan::with('admin')->where('user_id', '=', Auth::user()->id)->get();

        return response()->json($data);
    }

    public function changestatuspemesanan(Request $request)
    {
        $data = pemesanan::findByUuid($request->uuid);
        
        if($request->status == '3'){
            bengkel_details::where('user_id', '=', $data->admin_id)->first()->update([
                'status' => '1'
            ]);
        }
        $data->update(['status' => $request->status]);

        if(!$data){
            return response()->json(['pesan' => 'gagal'], 400);
        } else {
            return response()->json(['pesan' => 'sukses']);
        }
    }
    
    public function getuserdetail()
    {
        $data = User::findByUuid(Auth::user()->uuid, 'bengkel_detail');
        if(!$data){
            return response()->json(['pesan' => 'gagal'], 400);
        } else {
            return response()->json(['pesan' => 'sukses', 'data' => $data]);
        }
    }

}
