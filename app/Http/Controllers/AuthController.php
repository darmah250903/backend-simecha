<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\bengkel_details;
use JWTAuth, AppHelper;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Validator, DB, Hash, Mail, Auth, File, Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Password;
use App\Mail\DrakeEmail;
use App\Models\BengkelType;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use PhpParser\Node\Expr\Cast\Array_;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        $credentials = $request->only('credential', 'password');


        $rules = [
            'credential' => 'required',
            'password' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->messages()->first()], 400);
        }

        $test = str_split($request->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        } else {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }

        $confirm = User::where($check, '=', $credentials[$check])->first();
        // $oioi = !password_verify($credentials['password'], $confirm['password']);
        if (!password_verify($credentials['password'], $confirm['password'])) {
            return response()->json(['status' => false, 'message' => $check . '/password salah'], 404);
        } else if ($confirm['confirm'] == '0') {
            return response()->json(['status' => false, 'verification' => true, 'message' => 'Phone Email belum dikonfirmasi.'], 404);
        }
        // $credentials['is_verified'] = 1;

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => false, 'message' => $check . '/Password salah!'], 404);
            }
        } catch (JWTException $e) {
            return response()->json(['status' => false, 'message' => 'Sesuatu error terjadi.'], 500);
        }
        return response()->json(['status' => true, 'token' => $token, 'level' => $confirm->level, 'uuid' =>$confirm->uuid], 200)->header('Authorization', $token);
    }

    public function changepassword(Request $request)
    {
        $data = User::findByUuid(Auth::user()->uuid);
        $status = $data->update([
            'password' => Hash::make($request->pass)
        ]);
        if($status){
            return response()->json(['status' => true, 'data' => $data]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function changephoto(Request $request)
    {
        $user = User::findByUuid(Auth::user()->uuid);

        if(file_exists(public_path().'/images/foto_user/'.$user->foto)){
            File::delete('images/foto_user/'.$user->foto);
        }
        
        $decoded = base64_decode($request->foto);
        

        $extension = 'jpg';
        $FileName = Str::random(30).'.'.$extension;
        $path = public_path().'/images/foto_user/'.$FileName;

        file_put_contents($path, $decoded);

        $status = $user->update([
            'foto' => $FileName
        ]);

        if($status){
            return response()->json(['pesan' => 'sukses', 'status' => true]);
        } else {
            return response()->json(['pesan' => 'gagal', 'status' => false]);
        }

    }

    public function check(Request $req)
    {
        $credentials = $req->only(['credential']);

        $test = str_split($req->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }

        $oioi = User::where($check, '=', $credentials[$check])->first();

        if (empty($oioi)) {
            return response()->json(['status' => false, 'message' => $check . ' tidak terdaftar'], 404);
        } else {
            return response()->json(['status' => false, 'message' => $check . ' terdaftar'], 200);
        }
    }

    public function checkSignUp(Request $req)
    {
        $credentials = $req->only(['credential']);

        $test = str_split($req->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }

        $oioi = User::where($check, '=', $credentials[$check])->first();

        if (empty($oioi)) {
            return response()->json(['status' => false, 'message' => $check . ' oke'], 200);
        } else {
            return response()->json(['status' => false, 'message' => $check . '  sudah terdaftar'], 400);
        }
    }

    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate($request->bearerToken());
            return response()->json(['status' => true, 'message' => "Berhasil logout."]);
        } catch (JWTException $e) {
            return response()->json(['status' => false, 'message' => 'Sesuatu error terjadi.'], 500);
        }
    }

    public function clearcode(Request $req)
    {
        $credentials = $req->only(['credential']);

        $test = str_split($req->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }

        $oioi = User::where($check, '=', $credentials[$check])->first();

        $oioi->update(['verificationcode' => null]);

        return response()->json(['message' => 'reset verification'], 200);
    }

    public function back(Request $request)
    {
        $credentials = $request->only(['credential']);

        $test = str_split($request->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }

        $oioi = User::where($check, '=', $credentials[$check])->first();
        $oioi->delete();

        return response()->json(['message' => 'back'], 200);
    }

    public function verifikasi(Request $req)
    {
        $credentials = $req->only(['credential', 'code']);

        $test = str_split($req->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }

        $oioi = User::where($check, '=', $credentials[$check])->first();

        if ($oioi->verificationcode == $credentials['code']) {
            $oioi->update(['confirm' => '1']);
            return response()->json(['message' => 'berhasil mengkonfirmasi akun'], 200);
        } else {
            return response()->json(['status' => false, 'message' => 'kode vefifikasi salah'], 500);
        }
    }
    public function verifikasipassword(Request $req)
    {
        $credentials = $req->only(['code']);

        if(Auth::user()->phone == null){
            $oioi = User::where('email', '=', Auth::user()->email)->first();
        } else {
            $oioi = User::where('phone', '=', Auth::user()->phone)->first();
        }

        if ($oioi->verificationcode == $credentials['code']) {
            return response()->json(['message' => 'berhasil mengkonfirmasi akun'], 200);
        } else {
            return response()->json(['status' => false, 'message' => 'kode vefifikasi salah'], 500);
        }
    }

    public function accForgot(Request $req)
    {
        $credentials = $req->only(['credential', 'password1']);

        $test = str_split($req->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }

        $oioi = User::where($check, '=', $credentials[$check])->first();

        if (password_verify($credentials['password1'], $oioi['password'])) {
            return response()->json(['status' => false, 'message' => 'password baru sama dengan yang lama'], 500);
        } else {
            $oioi->update(['password' => Hash::make($credentials['password1'])]);
            return response()->json(['status' => false, 'message' => 'password baru sama dengan yang lama'], 200);
        }
    }

    public function sendVerification(Request $req)
    {
        $credentials = $req->only('credential');

        $test = str_split($req->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        } else {
            return response()->json(['status' => false, 'message' => 'Mohon Masukan Email/phone yang benar'], 500);
        }

        $oioi = User::where($check, '=', $credentials[$check])->first();



        if (empty($oioi)) {
            return response()->json(['status' => false, 'message' => 'Email/Phone Belum terdaftar'], 500);
        } else {

            $code = rand(100000, 999999);
            $datamail = array();
            $datamail['code'] = $code;
            $datamail['name'] = $oioi['name'];

            $oioi->update(['verificationcode' => $code]);


            if ($check == 'phone') {
                $client = new Client();

                $post = $client->request('POST', 'http://localhost:9000/send-message', [
                    'form_params' => [
                        'number' => $credentials[$check],
                        'message' => '*DRAKE*' . PHP_EOL . 'Kode verifikasi Anda adalah *' . $code . '*. RAHASIAKAN kode Anda. Abaikan Jika Anda tidak meminta kode verifikasi ini.',
                    ]
                ]);
                return response()->json(['status' => false, 'message' => 'Kode verifikasi Telah terkirim silahkan check whatsapp anda'], 200);
            } else {
                Mail::to($credentials[$check])->send(new DrakeEmail($datamail));
                return response()->json(['status' => false, 'message' => 'Kode verifikasi Telah terkirim silahkan check Email anda'], 200);
            }
        }
    }
    public function getcodeverifikasipassword()
    {

        

        if(Auth::user()->phone == null){
            $oioi = User::where('email', '=', Auth::user()->email)->first();
            $check = 'email';
        } else {
            $oioi = User::where('phone', '=', Auth::user()->phone)->first();
            $check = 'phone';
        }


        if (empty($oioi)) {
            return response()->json(['status' => false, 'message' => 'Email/Phone Belum terdaftar'], 500);
        } else {

            $code = rand(100000, 999999);
            $datamail = array();
            $datamail['code'] = $code;
            $datamail['name'] = $oioi->name;

            $oioi->update(['verificationcode' => $code]);

                // return response()->json(['status' => false, 'message' => 'Kode verifikasi Telah terkirim silahkan check Email anda'], 200);

            if ($check == 'phone') {
                $client = new Client();

                $post = $client->request('POST', 'http://localhost:9000/send-message', [
                    'form_params' => [
                        'number' => Auth::user()->phone,
                        'message' => '*DRAKE*' . PHP_EOL . 'Kode verifikasi Anda adalah *' . $code . '*. RAHASIAKAN kode Anda. Abaikan Jika Anda tidak meminta kode verifikasi ini.',
                    ]
                ]);
                return response()->json(['status' => false, 'message' => 'Kode verifikasi Telah terkirim silahkan check whatsapp anda'], 200);
            } else {
                Mail::to(Auth::user()->email)->send(new DrakeEmail($datamail));
                return response()->json(['status' => false, 'message' => 'Kode verifikasi Telah terkirim silahkan check Email anda'], 200);
            }
        }
    }

    public function register(Request $request)
    {
        $credentials = $request->only('credential', 'password', 'nama', 'type');

        $rules = [
            'credential' => 'required|max:255',
            // 'username' => 'required|max:255|unique:users',
            'password' => 'required|min:8',
            'nama' => 'required',
            // 'lastName' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->messages()->first()], 400);
        }

        $test = str_split($request->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        } else {
            return response()->json(['status' => false, 'message' => 'Mohon Masukan Email/phone yang benar'], 500);
        }


        $name = $request->nama;
        $credential = $credentials[$check];
        $password = $request->password;
        $type = $request->type;
        // $username = $request->username;

        if ($request->type == 'admin') {
            User::create([
                'name' => $name,
                $check => $credential,
                'level' => $type,
                'password' => Hash::make($password),
            ]);
            $user = User::where($check, '=' , $credential)->first();

            bengkel_details::create([
                'kota' => $request->kota,
                'provinsi' => $request->provinsi,
                'kode_pos' => $request->kode_pos,
                'alamat' => $request->alamat,
                'bengkel_type_id' => $request->bengkel_type,
                'lat' => $request->lat,
                'long' => $request->long,
                'user_id' => $user->id,
            ]);

        } else {

            User::create([
                'name' => $name,
                $check => $credential,
                'password' => Hash::make($password),
            ]);
        }
    }

    public function verifyUser($verification_code)
    {
        $check = DB::table('user_verifications')->where('token', $verification_code)->first();
        if (!is_null($check)) {
            $user = User::find($check->user_id);
            if ($user->is_verified == 1) {
                return response()->json([
                    'status' => false,
                    'message' => 'Alamat Email sudah diverifikasi.'
                ], 400);
            }
            $user->update(['is_verified' => 1]);
            DB::table('user_verifications')->where('token', $verification_code)->delete();
            return response()->json([
                'status' => true,
                'message' => 'Alamat email berhasil diverifikasi.'
            ]);
        }
        return response()->json(['status' => false, 'message' => "Kode verifikasi tidak valid."], 400);
    }

    public function recover(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            $error_message = "Alamat email Anda tidak ditemukan.";
            return response()->json(['status' => false, 'message' =>  $error_message], 404);
        }

        try {
            $token = AppHelper::generateForgotToken();

            $cek = DB::table('password_resets')->where('email', $request->email)->first();

            if (isset($cek->email)) {
                $ubahStatus = [
                    'token' => $token,
                ];
                DB::table('password_resets')->where('email', $request->email)->update($ubahStatus);
            } else {
                DB::table('password_resets')->insert(
                    [
                        'email' => $request->email,
                        'token' => $token,
                        'created_at' => new \DateTime(),
                    ]
                );
            }


            $name = $user->name;
            $email = $user->email;
            $subject = "Lupa Password?";

            Mail::send(
                'emails.forgot',
                ['name' => $name, 'token' => $token],
                function ($mail) use ($email, $name, $subject) {
                    $mail->from(getenv('MAIL_USERNAME'), "Dinas Koperasi Usaha Kecil & Menengah");
                    $mail->to($email, $name);
                    $mail->subject($subject);
                }
            );
        } catch (\Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['status' => false, 'message' => $error_message], 401);
        }
        return response()->json([
            'status' => true, 'message' => 'Tautan untuk reset password telah dikirim via email.'
        ]);
    }

    public function checkRecover(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {

            $getToken = DB::table('password_resets')
                ->where('token', $request->token)
                ->first();

            if (!is_null($getToken)) {
                return response()->json(['status' => true, 'message' => "Token valid."]);
            }

            return response()->json(['status' => false, 'message' => "Token tidak valid/expired."], 400);
        } else {
            abort(404);
        }
    }

    public function postRecover(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $rules = [
                'password' => 'required|min:6',
                'repassword' => 'required|min:6',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->messages()->first()], 400);
            }

            $forgetPassword = DB::table('password_resets')
                ->where('token', $request->input('token'))
                ->first();

            if (!is_null($forgetPassword)) {
                $user = \App\Models\User::where('email', $forgetPassword->email)->first();
                $user->password = bcrypt($request->password);

                if ($user->update()) {
                    DB::table('password_resets')->where('token', $request->token)->delete();
                    return response()->json(['status' => true, 'message' => "Berhasil membuat password baru."]);
                }
            }

            return response()->json(['status' => false, 'message' => "Token tidak valid/expired."], 400);
        } else {
            abort(404);
        }
    }

    public function user(Request $request)
    {
        $user = User::find(JWTAuth::user()->id);
        return response()->json([
            'status' => true,
            'data' => $user
        ]);
    }
    public function karyawan(Request $request)
    {
        $user = User::find(JWTAuth::karyawan()->id);
        return response()->json([
            'status' => true,
            'data' => $user
        ]);
    }
    public function magang(Request $request)
    {
        $user = User::find(JWTAuth::magang()->id);
        return response()->json([
            'status' => true,
            'data' => $user
        ]);
    }

    public function refresh()
    {
        if ($token = JWTAuth::refresh(JWTAuth::getToken())) {
            return response()
                ->json(['status' => true], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['status' => false, 'message' => 'refresh_token_error'], 401);
    }

    public function testing(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);

            // Get User By Search And Per Page
            $user = User::where(function ($q) use ($request) {
                $q->where('name', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('email', 'LIKE', '%' . $request->search . '%');
            })->orderBy('id', 'asc')->paginate($per);

            // Add Columns
            $user->map(function ($a) {
                if($a->block == '0'){
                    $a->action = '<span class="btn btn-sm btn-danger btn-icon btn-icon-md btn-block" title="block" data-id="' . $a->uuid . '"><i class="la la-lock kt-font-info"></i></span>';
                } else {
                    $a->action = '<span class="btn btn-sm btn-success btn-icon btn-icon-md btn-un-block" title="un-block" data-id="' . $a->uuid . '"><i class="la la-unlock kt-font-info"></i></span>';
                }
                return $a;
            });
            return response()->json($user);
        } else {
            abort(404);
        }
    }

    public function block($uuid)
    {
        $user = User::findByUuid($uuid);
        $user->update([
            'block' => '1'
        ]);
        
        return response()->json(['pesan' => "berhasil block user $user->name"]);
    }

    public function unblock($uuid)
    {
        $user = User::findByUuid($uuid);
        $user->update([
            'block' => '0'
        ]);
        
        return response()->json(['pesan' => "berhasil un-block user $user->name"]);
    }

    public function getdatabengkel()
    {
        $data = BengkelType::get(["nama", "id"]);
        return response()->json($data);
    }
}
