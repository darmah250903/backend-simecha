<?php

namespace App\Http\Controllers;

use App\Helpers\AppHelper as AH;
use App\Models\bengkel_details;
use App\Models\Chat;
use App\Models\pemesanan;
use App\Models\Rating;
use App\Models\Report;
use App\Models\Tambahan;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;

class PemesananController extends Controller
{
    public function adminGetDataPemesanan(Request $request)
    {

        $credentials = $request->only('credential');

        $test = str_split($request->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        } else {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }
        $status = $request->status;
        $data = User::Where($check, '=', $request->credential)->with(['pemesananAdmin' => function($q) use ($status){
            $q->where('status', $status);
        }])->get();





        return Response()->json(['data' =>  $data[0]->pemesananAdmin]);
    }

    public function adminGetData(Request $req)
    {
        $credentials = $req->only('credential');

        $test = str_split($req->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        } else {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }

        $data = User::with('bengkel_detail')->where($check, $req->credential)->first();

        return Response()->json($data);
    }

    public function adminChangeStatus(Request $req)
    {
        $credentials = $req->only('credential');

        $test = str_split($req->credential);
        $phone = $test[0] . $test[1];

        if ($phone == '08' || $phone == '62') {
            $credentials['phone'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'phone';
        } else if (in_array("@", $test) && in_array(".", $test)) {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        } else {
            $credentials['email'] = $credentials['credential'];
            unset($credentials['credential']);
            $check = 'email';
        }

        $data = User::where($check, $req->credential)->first();
        $status = bengkel_details::where('user_id', '=', $data->id)->first();
        if($status->status == "1"){
            $status->update([
                'status' => "0"
            ]);
            if($status){
                return response()->json([
                    'sukses'
                ], 200);
            } else {
                return response()->json([
                    'gagal'
                ], 400);
            }
        } else {
            $status->update([
                'status' => "1"
            ]);
            if($status){
                return response()->json([
                    'sukses'
                ], 200);
            } else {
                return response()->json([
                    'gagal'
                ], 400);
            }
        }
    }

    public function pesanan($status)
    {
        $data = pemesanan::with(['user', 'admin'])->where([['admin_id', '=', Auth::user()->id], ['status', '=', $status]])->get();


        return response()->json($data);
    }

    public function changestatuspemesanan(Request $request)
    {
        $data = pemesanan::findByUuid($request->uuid);

        if($request->status == '2'){
            $harga_total = 0;
            for ($i=0; $i < count($data->pemesananservice); $i++) { 
                $harga_total = $harga_total + $data->pemesananservice[$i]->harga;
            }
            $data->update(['harga_pemesanan'  => $harga_total+$data->harga_perjalanan]);
            bengkel_details::where('user_id', '=', $data->admin_id)->first()->update([
                'status' => '1'
            ]);
        }
        $data->update(['status'  => $request->status]);
        
        if(!$data){
            return response()->json(['pesan' => 'gagal'], 400);
        } else {
            return response()->json(['pesan' => 'sukses']);
        }
    }

    public function detailpesanan(Request $request)
    {
        $data = pemesanan::with(['user', 'admin', 'pemesananservice', 'tambahan'])->where('id', '=', $request->pesanan_id)->first();
        $chat = Chat::where('pesanan_id', '=', $request->pesanan_id)->get();
        $chat->map(function($q){
            if($q->sendBy == Auth::user()->level){
                $q->sendByMe = true;
            } else {
                $q->sendByMe = false;
            }
        });

        if(count($data->pemesananservice) != 0){
            $data->total_harga = 0;
            
            for ($i=0; $i < count($data->pemesananservice); $i++) { 
                $data->total_harga +=  $data->pemesananservice[$i]->harga;
            }
            
            
            if($data->status != '2'){
                $data->total_harga = AH::rupiah($data->total_harga + $data->harga_perjalanan);
            } else {
                $data->total_harga = AH::rupiah($data->harga_pemesanan + $data->harga_perjalanan);
            }
            
            if($data->tambahan != null){
                $data->tambahan->harga = AH::rupiah($data->tambahan->harga + $data->harga_perjalanan);
            }
        } else {
                $data->total_harga = AH::rupiah($data->harga_pemesanan);
        }

        $data->pemesananservice->map(function($q){
            $q->harga = AH::rupiah($q->harga);
            });

        $oi = ['nama' => 'Ongkos Jalan', 'harga' => AH::rupiah($data->harga_perjalanan)];

        $data->pemesananservice->push($oi);
        $data->harga_perjalanan = AH::rupiah($data->harga_perjalanan);

        return response()->json([
            'data' => $data, 'chat' => $chat
        ]);
    }

    public function selesaipemesanantambahan(Request $request)
    {
        $data = pemesanan::with(['user', 'admin', 'pemesananservice', 'admin.bengkel_detail'])->where('uuid', '=', $request->uuid)->first();
        
        $harga_total = intval($request->form['harga']);
        
        for ($i=0; $i < count($data->pemesananservice); $i++) { 
            $harga_total = $harga_total + $data->pemesananservice[$i]->harga;
        }
            // return response()->json(['pesan' => $harga_total]);
        if($data->pemesanan_type == '0'){
            $data->update(['deskripsi' => $request->form['deskripsi'], 'harga_pemesanan' => $harga_total + $data->harga_perjalanan, 'status' => $request->status]);
        } else {
            $data->update(['harga_pemesanan' => $harga_total, 'status' => $request->status]);
            Tambahan::create([
                'harga' => intval($request->form['harga']),
                'deskripsi' => $request->form['deskripsi'], 
                'pemesanan_id' => $data->id
            ]);
        }


        if(!$data){
            return response()->json(['pesan' => 'gagal'], 400);
        }else{
            bengkel_details::findByUuid($data->admin->bengkel_detail->uuid)->update([
                'status' => '1'
            ]);
            return response()->json(['pesan' => 'sukses']);
        }
        
    }

    public function inputUlang(Request $request)
    {
        
        $data = pemesanan::where('id', '=', $request->id)->first();

        $status = $data->update([
            'status' => '1'
        ]);

        if($status){
            return response()->json([
                'pesan' => 'sukses'
            ]);
        } else {
            return response()->json([
                'pesan' => 'gagal'
            ], 500);
        }
    }

    public function sendReport(Request $request)
    {

        
        $data = Report::create([
            'pemesanan_id' => $request->pemesanan_id,
            'user_id' => Auth::user()->id,
            'deskripsi' => $request->deskripsi,
        ]);
        return response()->json([
            'pesan' => 'terkirim'
        ]);
    }
    
    public function sendRating(Request $request)
    {
        
        // return response()->json([
        //     'pesan' => $request
        // ]);
        $data = Rating::create([
            'pemesanan_id' => $request->pemesanan_id,
            'user_id' => Auth::user()->id,
            'deskripsi' => $request->deskripsi,
            'score' => $request->score,
        ]);

        if($data){
            $update = pemesanan::where('id', '=', $request->pemesanan_id)->first()->update(['user_confirm' => '1']);

            if($update){
                return response()->json([
                    'pesan' => 'terkirim'
                ]);
            } else {
                return response()->json([
                    'pesan' => 'gagal'
                ], 500);

            }

        } else {
            return response()->json([
                'pesan' => 'gagal'
            ], 500);
        }
        
    }

    public function ubahDetail(Request $request)
    {
        $data = User::findByUuid(Auth::user()->uuid);
        $status = $data->update([
            'name' => $request->nama
        ]);

        if($status){
            return response()->json(['msg' => 'sukses']);
        } else {
            return response()->json(['msg' => 'gagal']);
        }
    }
    public function ubahPosisi(Request $request)
    {
        $data = bengkel_details::where('user_id', '=', Auth::user()->id)->first();
        // return response()->json(['msg' => $data]);
        $status = $data->update([
            'lat' => $request->lat,
            'long' => $request->long
        ]);

        if($status){
            return response()->json(['msg' => 'sukses']);
        } else {
            return response()->json(['msg' => 'gagal']);
        }
    }
}

