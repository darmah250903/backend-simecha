<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Helpers\AppHelper as AH;
use App\Models\BengkelType;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);

            // Get User By Search And Per Page
            $data = Service::with('bengkeltype')->where(function ($q) use ($request) {
                $q->where('nama', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('harga', 'LIKE', '%' . $request->search . '%');
            })->orWhereHas('bengkeltype', function($q) use ($request){
                $q->where('nama', 'LIKE', '%' . $request->search . '%');
            })->orderBy('id', 'asc')->paginate($per);

            $data->map(function ($a) {
                    $a->action = '<span class="btn btn-sm btn-danger btn-icon btn-icon-md btn-hapus" title="hapus" data-id="' . $a->uuid . '"><i class="la la-trash kt-font-info"></i></span><span class="btn btn-sm btn-warning ml-2 btn-icon btn-icon-md btn-edit" title="edit" data-id="' . $a->uuid . '"><i class="la la-pencil kt-font-info"></i></span>';
                    $a->harga = AH::rupiah($a->harga);
                return $a;
            });
            return response()->json($data);
        } else {
            abort(404);
        }
    }

    public function getdataedit($uuid)
    {
        $data = Service::findByUuid($uuid);
        
        return response()->json($data);
    }
    
    public function getdata()
    {
        return response()->json(BengkelType::get());
    }

    public function tambah(Request $request)
    {
        $data = Service::create([
            'nama' => $request->data['nama'],
            'harga' => $request->data['harga'],
            'bengkel_type_id' => $request->data['bengkel_id'],
        ]);

        if($data){
            return response()->json(['pesan' => 'sukses menambah service']);
        } else {
            return response()->json(['pesan' => 'gagal menambah service'], 400);
        }
    }

    public function delete($uuid)
    {
        $data = Service::findByUuid($uuid);
        $data->delete();

        return response()->json(['pesan' => 'sukses menghapus']);
    }

    public function edit(Request $request)
    {
        $data = Service::findByUuid($request->data['uuid']);
        
        $data->update([
            'nama' => $request->data['nama'],
            'harga' => $request->data['harga'],
            'bengkel_type_id' => $request->data['bengkel_id'],
        ]);
        return response()->json(['pesan' => 'sukses mengedit service']);

    }
}
