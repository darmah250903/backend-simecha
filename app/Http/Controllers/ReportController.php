<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);

            // Get User By Search And Per Page
            $data = Report::with(['user', 'pemesanan'])->where(function ($q) use ($request) {
                $q->Where('deskripsi', 'LIKE', '%' . $request->search . '%');
            })->orWhereHas('user', function($q) use ($request){
                $q->Where('name', 'LIKE', '%' . $request->search . '%');
            })->orWhereHas('pemesanan', function($q) use ($request){
                $q->where('kd_pemesanan', 'LIKE', '%' . $request->search . '%');
            })->orderBy('id', 'asc')->paginate($per);

            // Add Columns
            $data->map(function ($a) {
                    $a->action = '<span class="btn btn-sm btn-clean btn-icon btn-icon-md btn-detail" title="detail" data-id="' . $a->uuid . '"><i class="la la-eye kt-font-info"></i></span>';
                return $a;
            });
            return response()->json($data);
        } else {
            abort(404);
        }
    }

    public function detail($uuid)
    {
        $data = Report::findByUuid($uuid);

        return response()->json($data);
    }
}
