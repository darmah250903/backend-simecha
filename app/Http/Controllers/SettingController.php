<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Validator;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function show()
    {
        if (request()->wantsJson() && request()->ajax()) {
            
            $config = Setting::first(['name', 'background', 'logo_dark', 'logo_white', 'description']);

            return response()->json([
                'status' => true,
                'data' => $config
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => '404 Not Found.'
            ], 404);
        }
    }

    public function update(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {

            $rules = [
                'name' => 'required',
                'description' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()) {
                return response()->json([
                    'status'=> false,
                    'message' => $validator->messages()->first()
                ], 500);
            }
            
            $config = Setting::first();

            if(isset(request()->background_file)){
                $rules = [
                    'background_file' => 'required|mimes:jpg,png,jpeg|max:5120',
                ];

                $validator = Validator::make($request->all(), $rules);

                if($validator->fails()) {
                    return response()->json([
                        'status'=> false,
                        'message' => $validator->messages()->first()
                    ], 500);
                }

                if(is_file(public_path('images/background/').$config->background)){
                    unlink(public_path('images/background/').$config->background);
                }
                $name = 'BACKGROUND_'.time().'.'.request()->background_file->getClientOriginalExtension();
                request()->background_file->move(public_path('images/background'), $name);
                $request['background'] = $name;
            }

            if(isset(request()->logo_dark_file)){
                $rules = [
                    'logo_dark_file' => 'required|mimes:jpg,png,jpeg|max:5120',
                ];

                $validator = Validator::make($request->all(), $rules);

                if($validator->fails()) {
                    return response()->json([
                        'status'=> false,
                        'message' => $validator->messages()->first()
                    ], 500);
                }

                if(is_file(public_path('images/logo/').$config->logo_dark)){
                    unlink(public_path('images/logo/').$config->logo_dark);
                }
                $name = 'LOGO_DARK_'.time().'.'.request()->logo_dark_file->getClientOriginalExtension();
                request()->logo_dark_file->move(public_path('images/logo'), $name);
                $request['logo_dark'] = $name;
            }

            if(isset(request()->logo_white_file)){
                $rules = [
                    'logo_white_file' => 'required|mimes:jpg,png,jpeg|max:5120',
                ];

                $validator = Validator::make($request->all(), $rules);

                if($validator->fails()) {
                    return response()->json([
                        'status'=> false,
                        'message' => $validator->messages()->first()
                    ], 500);
                }

                if(is_file(public_path('images/logo/').$config->logo_white)){
                    unlink(public_path('images/logo/').$config->logo_white);
                }
                $name = 'LOGO_WHITE_'.time().'.'.request()->logo_white_file->getClientOriginalExtension();
                request()->logo_white_file->move(public_path('images/logo'), $name);
                $request['logo_white'] = $name;
            }

            $config->update($request->all());

            return response()->json([
                'status' => true,
                'data' => $config
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => '404 Not Found.'
            ], 404);
        }
    }
}
