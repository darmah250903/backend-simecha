<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use Illuminate\Http\Request;
use Auth;

class ChatController extends Controller
{
    public function send(Request $request)
    {
        $data = Chat::create([
            'pesanan_id' => $request->pemesanan_id, 
            'user_id' => $request->user_id, 
            'admin_id' => $request->admin_id, 
            'message' => $request->message, 
            'sendBy' => Auth::user()->level
        ]);

        return 'sukses';
    }
}
