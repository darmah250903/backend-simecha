<?php

namespace App\Helpers;
use Auth;
use LogicException;

class AppHelper
{

	static function randomColor($awal='rgb(', $akhir=')')
	{
		$rgbColor = [];

		foreach(array('r', 'g', 'b') as $color){
			$rgbColor[$color] = mt_rand(0, 255);
		}

		return $awal.implode(",", $rgbColor).$akhir;
	}

	static function cached_asset($path, $bustQuery = false)
	{
        $realPath = public_path($path);
        if ( ! file_exists($realPath)) {
            throw new LogicException("File not found at [{$realPath}]");
        }
        $timestamp = filemtime($realPath);

        if ( ! $bustQuery) {
            $extension = pathinfo($realPath, PATHINFO_EXTENSION);
            $stripped = substr($path, 0, -(strlen($extension) + 1));
            $path = implode('.', array($stripped, $timestamp, $extension));
        } else {
            $path  .= '?' . $timestamp;
        }
        return asset($path);
    }

	static function BulanToRomawi($date)
	{
		$array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
		$bln = $array_bln[$date];
		return $bln;
	}

    static function tanggal_indo($tanggal, $cetak_hari = false)
	{
		$hari = array ( 1 =>    'Senin',
					'Selasa',
					'Rabu',
					'Kamis',
					'Jumat',
					'Sabtu',
					'Minggu'
				);
				
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split 	  = explode('-', $tanggal);
		$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
		
		if ($cetak_hari) {
			$num = date('N', strtotime($tanggal));
			return $hari[$num] . ', ' . $tgl_indo;
		}
		return $tgl_indo;
	}

	static function tgl_indo($tgl){
     	$tanggal = substr($tgl,8,2);
     	switch (substr($tgl,5,2)){
					case '01': 
						$bulan= "Januari";
						break;
					case '02':
						$bulan= "Februari";
						break;
					case '03':
						$bulan= "Maret";
						break;
					case '04':
						$bulan= "April";
						break;
					case '05':
						$bulan= "Mei";
						break;
					case '06':
						$bulan= "Juni";
						break;
					case '07':
						$bulan= "Juli";
						break;
					case '08':
						$bulan= "Agustus";
						break;
					case '09':
						$bulan= "September";
						break;
					case '10':
						$bulan= "Oktober";
						break;
					case '11':
						$bulan= "November";
						break;
					case '12':
						$bulan= "Desember";
						break;
				}

		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;
     }

	static function hari_indo($tanggal)
	{
		$hari = array ( 1 =>    'Senin',
					'Selasa',
					'Rabu',
					'Kamis',
					'Jumat',
					'Sabtu',
					'Minggu'
				);
		$num = date('N', strtotime($tanggal));
		return $hari[$num];
	}

	static function tanpa_tahun($tanggal, $cetak_hari = false)
	{
		$hari = array ( 1 =>    'Senin',
					'Selasa',
					'Rabu',
					'Kamis',
					'Jumat',
					'Sabtu',
					'Minggu'
				);
				
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split 	  = explode('-', $tanggal);
		$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ];
		
		if ($cetak_hari) {
			$num = date('N', strtotime($tanggal));
			return $hari[$num] . ', ' . $tgl_indo;
		}
		return $tgl_indo;
	}

    static function TglToText($tgl)
    {
    	$split 	  = explode('-', $tgl);
    	$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
    	$hari = self::hari_indo($tgl);
		$tgl_indo = self::AngkaToText($split[2]);
		$bulan = $bulan[(int)$split[1]];
		$tahun = self::AngkaToText($split[0]);
		return ucfirst($hari)." tanggal ".ucwords($tgl_indo)." bulan ".ucfirst($bulan)." tahun ". ucwords($tahun);

    }

    static function AngkaToText($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = "". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = self::AngkaToText($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = self::AngkaToText($nilai/10)." puluh ". self::AngkaToText($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus " . self::AngkaToText($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = self::AngkaToText($nilai/100) . " ratus " . self::AngkaToText($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu " . self::AngkaToText($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = self::AngkaToText($nilai/1000) . " ribu " . self::AngkaToText($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = self::AngkaToText($nilai/1000000) . " juta " . self::AngkaToText($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = self::AngkaToText($nilai/1000000000) . " milyar " . self::AngkaToText(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = self::AngkaToText($nilai/1000000000000) . " trilyun " . self::AngkaToText(fmod($nilai,1000000000000));
		}     
		return ucwords($temp);
	}

	static function seo($s) {
	    $c = array (' ');
	    $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
	    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
	    $s = strtolower(str_replace($c, '-', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
	    
	    return $s;
	}

	static function filename($s) {
	    $c = array (' ');
	    $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
	    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
	    $s = strtoupper(str_replace($c, '_', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
	    
	    return $s;
	}

	static function generateRandomToken()
	{
		return self::generateRandomCharHex(8).'-'.self::generateRandomCharHex(4).'-'.self::generateRandomCharHex(4).'-'.self::generateRandomCharHex(4).'-'.self::generateRandomCharHex(12);
	}

	static function generateRandomCharHex($a) {
		$char = 'ABCDEF1234567890';
		$str = '';

		for ($i=0; $i < $a; $i++) { 
			$pos = rand(0, strlen($char)-1);
			$str .= $char{$pos};
		}

		return $str;
	}

	static function generateForgotToken()
	{
		$code = '';
        do {
        $code = strtolower(self::generateRandomCharHex(8).'-'.self::generateRandomCharHex(4).'-'.self::generateRandomCharHex(4).'-'.self::generateRandomCharHex(4).'-'.self::generateRandomCharHex(12));
        $cek=\DB::table('password_resets')->where('token',$code)->count();
        } while(0 < $cek);
        return $code;
	}

	static function rupiah($a, $b=0)
	{
		return "Rp. ".number_format($a,$b,',','.');;
	}
    
}
