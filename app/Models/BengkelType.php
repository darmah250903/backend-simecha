<?php

namespace App\Models;
use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class BengkelType extends Model
{
    use Uuid;

    protected $fillable = ['uuid', 'nama'];

    public function service()
    {
        return $this->hasMany(Service::class, 'bengkel_type_id', 'id');
    }
}
