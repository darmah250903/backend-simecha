<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Rating extends Model
{
    use Uuid;
    
    protected $fillable = ['uuid', 'pemesanan_id', 'user_id', 'score' , 'deskripsi'];

    public function pemesanan()
    {
        return $this->belongsTo(pemesanan::class, 'pemesanan_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
