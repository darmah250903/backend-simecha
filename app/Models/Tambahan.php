<?php

namespace App\Models;
use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Tambahan extends Model
{
    use Uuid;
    protected $fillable = ['uuid', 'pemesanan_id', 'deskripsi', 'harga'];
}
