<?php

namespace App\Models;
use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class bengkel_details extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'kota', 'provinsi', 'kode_pos', 'alamat', 'bengkel_type_id', 'status', 'user_id', 'lat', 'long'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\user', 'user_id', 'id');
    }

}
