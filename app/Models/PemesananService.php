<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class PemesananService extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'pemesanan_id', 'service_id'
    ];
}
