<?php
namespace App\Models;
use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class pemesanan extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'kd_pemesanan', 'lat', 'long','harga_pemesanan', 'user_id', 'admin_id', 'tgl_pemesanan', 'pemesanan_type', 'status' ,'alamat', 'deskripsi', 'deskripsi_user', 'user_confirm', 'harga_perjalanan'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    public function pemesananservice()
    {
        return $this->belongsToMany(Service::class, 'pemesanan_services', 'pemesanan_id', 'service_id');
    }

    public function tambahan(){
        return $this->hasOne(Tambahan::class, 'pemesanan_id', 'id');
    }
}
