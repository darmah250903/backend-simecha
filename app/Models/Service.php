<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Service extends Model
{
    use Uuid;

    protected $fillable = ['uuid', 'bengkel_type_id', 'nama', 'harga'];

    public function bengkeltype()
    {
        return $this->hasOne(BengkelType::class, 'id', 'bengkel_type_id');
    }
}
