<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'name', 'email', 'password', 'level', 'verificationcode', 'username', 'phone', 'confirm', 'foto', 'block'
    ];

    protected $appends = [
        'foto_dir'
    ];
    
    function getFotoDirAttribute(){
        if(!$this->foto || !is_file(public_path('images/foto_user/'.$this->foto))) {
            return asset('/images/foto_user/avatar.jpg');
        }
        return asset('images/foto_user/'.$this->foto);
    }

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function pemesananAdmin()
    {
        return $this->hasMany(pemesanan::class, 'admin_id', 'id');
    }

    public function pemesananUser()
    {
        return $this->hasMany(pemesanan::class, 'user_id', 'id');
    }

    public function bengkel_detail()
    {
        return $this->hasOne(bengkel_details::class, 'user_id', 'id');
    }
}
