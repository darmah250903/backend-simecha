<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Chat extends Model
{
    use Uuid;

    protected $fillable = ['uuid', 'pesanan_id', 'user_id', 'admin_id', 'message', 'sendBy'];

    public function pesanan()
    {
        return $this->belongsTo(pemesanan::class, 'pesanan_id','id');
    }

    public function admin()
    {
        return $this->belongsTo(pemesanan::class, 'admin_id','id');
    }

    public function user()
    {
        return $this->belongsTo(pemesanan::class, 'user_id','id');
    }
    
}
