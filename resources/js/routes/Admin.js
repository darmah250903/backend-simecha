const Admin = [
    // START ADMIN //
    {
        name: 'admin',
        path: '/admin',
        component: () => import(/* webpackChunkName: "admin" */ '../pages/admin/home/Index'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['Admin',  'Dashboard']
        }
    },
    {
        name: 'admin.setting',
        path: '/admin/setting',
        component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/setting/Index'),
        meta: {
            title: `Setting`,
            auth: true,
            breadcrumb: ['Admin', 'Setting']
        }
    },
    // END ADMIN //
];

export default Admin