const Magang = [
    // START USER //
    {
        name: 'magang',
        path: '/magang',
        component: () => import(/* webpackChunkName: "magang" */ '../pages/magang/home/Index'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['magang',  'Dashboard']
        }
    },
    // END USER //
];

export default Magang