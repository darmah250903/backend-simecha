const User = [
    // START USER //
    {
        name: 'user',
        path: '/user',
        component: () => import(/* webpackChunkName: "user" */ '../pages/user/home/Index'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['User',  'Dashboard']
        }
    },
    // END USER //
];

export default User