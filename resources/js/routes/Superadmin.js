const Superadmin = [
    // START USER //
    {
        name: 'superadmin',
        path: '/superadmin',
        component: () =>
            import ( /* webpackChunkName: "superadmin" */ '../pages/superadmin/home/Index'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['superadmin', 'Dashboard']
        }
    },
    {
        name: 'settings',
        path: '/superadmin/settings',
        component: () =>
            import ( /* webpackChunkName: "superadmin" */ '../pages/superadmin/setting/Index'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['superadmin', 'Dashboard']
        }
    },
    {
        name: 'service',
        path: '/superadmin/service',
        component: () =>
            import ( /* webpackChunkName: "superadmin" */ '../pages/superadmin/service/Index'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['superadmin', 'Dashboard']
        }
    },
    {
        name: 'report',
        path: '/superadmin/report',
        component: () =>
            import ( /* webpackChunkName: "superadmin" */ '../pages/superadmin/report/Index'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['superadmin', 'Dashboard']
        }
    },
    // END USER //
];

export default Superadmin